##buscamos desequilibrio de ligamiento entre SNP de región de BUD13, ZNF259, APOA5-A4-C3-A1, SIRT3, SIDT2
##tomamos SNP reportados y el encontrado en nuestro estudio
cd ~/analisis_asociacion/convivencias_medix_qxob/conjunto_niños_adultos
bcftools view -i 'ID=@snp_SIDT2_APOA5.txt' ../adultos_mega/cirugia_bar/imputados/fusionados/adultos.032019.genoref.vcf.gz -Oz -o asociacion/locales/lipidos/sidt2_apoa5_adultos.vcf.gz
bcftools view -i 'ID=@snp_SIDT2_APOA5.txt' ../niños/imputados/fusionados/fusion_noviembre2018.genoref.vcf.gz -Oz -o asociacion/locales/lipidos/sidt2_apoa5_niños.vcf.gz

tabix asociacion/locales/lipidos/sidt2_apoa5_adultos.vcf.gz 
tabix asociacion/locales/lipidos/sidt2_apoa5_niños.vcf.gz
bcftools merge asociacion/locales/lipidos/sidt2_apoa5_adultos.vcf.gz asociacion/locales/lipidos/sidt2_apoa5_niños.vcf.gz | vcftools --vcf - --hap-r2 --out asociacion/locales/lipidos/sidt2_apoa5

bcftools query -f '%CHROM\t%ID\t%POS\n' asociacion/locales/lipidos/sidt2_apoa5_adultos.vcf.gz > SIDT2_APOA5.list

##copiado a tobago
cd ~/Documentos/ugepas/analisis_asociacion/agrupado
scp -P 7637 lmacias@muuk.inmegen.gob.mx:~/analisis_asociacion/convivencias_medix_qxob/conjunto_niños_adultos/asociacion/locales/lipidos/sidt2_apoa5.hap.ld SIDT2/manhaloc/

scp -P 7637 lmacias@muuk.inmegen.gob.mx:~/analisis_asociacion/convivencias_medix_qxob/conjunto_niños_adultos/SIDT2_APOA5.list SIDT2/manhaloc/


R
library(LDheatmap)
setwd("SIDT2/manhaloc")
gen <- "sidt2_apoa5"
origin <- paste0(gen, ".hap.ld")
ld_calc <- read.table(origin, header=TRUE)
ld_calc <- ld_calc[order(ld_calc$POS2),]
ld_calc <- ld_calc[order(ld_calc$POS1),]
seleccion <- read.csv("../GWAS_hdl_snpmenor1emenos5_112020.csv")
ld_calc <- ld_calc[ld_calc$POS1 %in% seleccion$BP[seleccion$CHR==11] & ld_calc$POS2 %in% seleccion$BP[seleccion$CHR==11],]
matriz_ld <- reshape(ld_calc, timevar="POS1", idvar="POS2", drop=c("CHR", "D", "Dprime", "N_CHR"), direction="wide")
rownames(matriz_ld) <- matriz_ld[,1]
matriz_ld <- matriz_ld[,-1]
matriz_ld <- rbind(c(1,matriz_ld[,1]), matriz_ld)
matriz_ld <- cbind(matriz_ld, nuevo=unlist(c(matriz_ld[nrow(matriz_ld),] ,1)))
rownames(matriz_ld)[1] <- gsub("R.2.", "", colnames(matriz_ld)[1])
colnames(matriz_ld)[ncol(matriz_ld)] <- paste0("R.2.", rownames(matriz_ld)[nrow(matriz_ld)])
matriz_ld <- as.matrix(matriz_ld)
all.equal(gsub("R.2.", "", colnames(matriz_ld)), rownames(matriz_ld))
diag(matriz_ld) <- 1

for (i in 3:nrow(matriz_ld))
	{
	matriz_ld[i-1, i:ncol(matriz_ld)] <- matriz_ld[i:nrow(matriz_ld),i-1]
	}

mapa <- read.table("plink.chr11.GRCh37.map")
mapa <- mapa[mapa$V4 %in% seleccion$BP,]
rownames(matriz_ld) <- mapa$V2[pmatch(rownames(matriz_ld), mapa$V4, duplicates.ok=TRUE)]
rgb.palette <- colorRampPalette(rev(c("white", "red")), space = "rgb")
snp <- mapa[,2]

pdf(paste0("ld_region_",gen,".pdf"))
	ld <- LDheatmap(matriz_ld, color=rgb.palette(18), geneMapLocation=0.1, geneMapLabelY=0.15, SNP.name=snp[c(5,13)], genetic.distances=mapa[,4])
	##ld <- LDheatmap.addGenes(ld, "chr11", genome="hg19", non_coding=FALSE)
dev.off()

write.csv(matriz_ld, "ld_asochdl_chr11.csv")
