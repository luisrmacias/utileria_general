cuali <- function(variable, desenlace, grupos, etiqueta=NULL, titulado=TRUE, con_n=TRUE, intervalo=TRUE, con_p=FALSE, trend=FALSE, datos)
{
  mula <- as.formula(paste(variable, "~", paste(grupos, collapse = " + ")))
  desagregado <- aggregate(mula, function(x) sum(x==desenlace)/sum(complete.cases(x)), data=datos)
  names(desagregado)[grep("EDAD >= 60", names(desagregado))] <- "grupo_edad"
  if (any(grepl("grupo_edad", names(desagregado))))
  {
    desagregado$grupo_edad <- factor(desagregado$grupo_edad, labels=c("Menores de 60 años", "60 años y más"))
  }
  temp <- aggregate(mula, function(x) sum(complete.cases(x)), data=datos)
  desagregado$n <- temp[[ncol(temp)]]
  valor <- desagregado[,grep(variable, names(desagregado))]
  desagregado <- data.frame(desagregado, l_inf = valor - 1.96*((valor * (1-valor))/desagregado$n))
  desagregado <- data.frame(desagregado, l_sup = valor + 1.96*((valor * (1-valor))/desagregado$n))
  desagregado[apply(desagregado, 2, function(x) all(x<1))] <- desagregado[apply(desagregado, 2, function(x) all(x<1))]*100
  desagregado[grep(variable, (names(desagregado)))] <- round(desagregado[grep(variable, names(desagregado))],2)
  desagregado[,c("l_inf", "l_sup")] <- round(desagregado[,c("l_inf", "l_sup")],1)
  if (intervalo)
  {
    desagregado[,length(grupos)+1] <- paste0(desagregado[,length(grupos)+1], " (", desagregado$l_inf, "-", desagregado$l_sup, ")")
  }
  names(desagregado)[length(grupos)+1] <- etiqueta
  desagregado <- data.frame(t(desagregado))
  names(desagregado) <- desagregado[length(grupos),]
  desagregado <- desagregado[-length(grupos),]
  if (length(grupos)>1)
  {
    if (!titulado)
    {
      desagregado <- desagregado[-1,]
    } 
    if (titulado & con_n)
    {
      desagregado[1,] <- paste0(" (n = ", str_trim(format(as.numeric(desagregado[1,]), big.mark = ",")), ")")
    }
    desagregado <- desagregado[1:(nrow(desagregado)-3),]
  } else if (length(grupos)==1)
  {
    if (con_n)
    {
      desagregado <- desagregado[c(2,1),]
      desagregado[1,] <- paste0("n = ", str_trim(format(as.numeric(desagregado[1,]), big.mark = ",")))
      rownames(desagregado)[1] <- ""
    } else if (!con_n)
    {
      desagregado <- desagregado[1:(nrow(desagregado)-3),]
    } 
  }
  if (con_p) 
  {
    if (!trend)
    {
      tabulated <- eval(parse(text = paste("with(datos, table(", variable, ",", grupos, "))")))
      p <- signif(chisq.test(tabulated)$p.value,4)
    } else if (trend) {
      x <- eval(parse(text = paste("with(", datos, "tapply(", variable, ",", grupos, ", function(x) sum(x==desenlace)))")))
      n <- eval(parse(text = paste("with(", datos, "table(", grupos, "))")))
      p <- signif(prop.trend.test(x, n)$p.value,4)
    }
    desagregado <- data.frame(desagregado, p=p)
    if (nrow(desagregado)>1) desagregado[1:nrow(desagregado)-1,ncol(desagregado)] <- ""
    desagregado
  } else if (!con_p) desagregado 
}
    
cuanti <- function(variable, grupos, etiqueta=NULL, titulado=TRUE, con_n=TRUE, intervalo=TRUE, con_p=FALSE, datos)
{
  mula <- as.formula(paste(variable, "~", paste(grupos, collapse = " + ")))
  desagregado <- aggregate(mula, function(x) quantile(x, c(0.25, 0.5, 0.75)), data=datos)
  names(desagregado)[grep("EDAD >= 60", names(desagregado))] <- "grupo_edad"
  if (any(grepl("grupo_edad", names(desagregado))))
  {
    desagregado$grupo_edad <- factor(desagregado$grupo_edad, labels=c("Menores de 60 años", "60 años y más"))
  }
  temp <- aggregate(mula, function(x) sum(complete.cases(x)), data=datos)
  desagregado$n <- temp[[ncol(temp)]]
  temp <- as.matrix(desagregado[,grep(variable, names(desagregado))])
  temp[,2] <- round(temp[,2], 2)
  temp[,-2] <- round(temp[,-2], 1)
  colnames(temp) <- paste(variable, colnames(temp), sep=".")
  desagregado <- desagregado[c(1:length(grupos), ncol(desagregado))]
  desagregado <- cbind(desagregado, temp)
  if (intervalo)
  {
    desagregado[,length(grupos)+2] <- paste0(desagregado[,length(grupos)+3], " (", desagregado[,length(grupos)+2], "-", desagregado[,length(grupos)+4], ")")
  }
  names(desagregado)[length(grupos)+2] <- etiqueta
  desagregado <- data.frame(t(desagregado))
  names(desagregado) <- desagregado[length(grupos),]
  desagregado <- desagregado[-length(grupos),]
  if (length(grupos)>1)
  {
    if (!titulado)
    {
      desagregado <- desagregado[length(grupos)+1,]
    }
    if(titulado & !con_n)
    {
      desagregado <- desagregado[c(1:(length(grupos)-1), length(grupos)+1),]
    }
    if (titulado & con_n)
    {
      desagregado[length(grupos)-1,] <- paste0(desagregado[length(grupos)-1,], " (n = ", str_trim(format(as.numeric(desagregado[length(grupos),]), big.mark = ",")), ")")  
      desagregado <- desagregado[c(1:(length(grupos)-1), length(grupos)+1),]
    }
  } else if (length(grupos)==1)
  {
    if (con_n)
    {
      desagregado <- desagregado[1:2,]
      desagregado[1,] <- paste0("n = ", str_trim(format(as.numeric(desagregado[1,]), big.mark = ",")))
      rownames(desagregado)[1] <- ""
    } else if (!con_n)
    {
      desagregado <- desagregado[2,]
    } 
  }
  if (con_p)
  	{
  	if (length(mula)==3)
  		{
  		p <- signif(kruskal.test(mula, datos)$p.value,4)
  		} else if (length(mula)>3) {p <- NA}
  		#queda pendiente el cálculo de p en situaciones más complejas
  	desagregado <- data.frame(desagregado, p=p)
  	if (nrow(desagregado)>1) desagregado[1:nrow(desagregado)-1,ncol(desagregado)] <- ""
  	desagregado
  	} else if (!con_p) desagregado
  }

tabulados_or <- function(dep, indep, grupos, etiq, data=adultos)
 {
  mula <- as.formula(paste(indep, "~", dep))
  fit <- glm(mula, family = "binomial", subset = grupos, data=data)
  ci <- round(exp(confint(fit)),2)[-1,]
  res <- data.frame(OR = round(exp(coef(fit))[-1], 2), CI = paste(ci[1], "-", ci[2]), p = signif(summary(fit)$coeff[-1,4], 3))
  rownames(res) <- etiq
  res
}
