##########################################################################################################
library(ggplot2)
library(reshape2)
library(forcats)
##library(coxme)
##queda pendiente calcular pes con modelo lineal mixto
##setwd("/export/home/lmacias/analisis_asociacion/adultos_mega/replica")
load("concentrado_replica.RData")
al <- list(c("C", "T"), c("G", "T"))
imc_bioq <- c("glucosa", "trigli", "hdl", "bmi", "smidf")
clave <- "urico"
fenos <- c(clave, imc_bioq)
##elegibles <- with(incluidos, DM=="No DM" & !endescubri)

##función que crea promedios x genotipos de 2 snp y combinados, no logramos generalizar fenotipos incluidos
datos_interaccion <- function(data, snps, alelos, mediciones, ids)
	{
	genotipos <- sapply(alelos, function(x) c(paste0(x[1], x[1]), paste0(x[1], x[2]), paste0(x[2], x[2])))
	condatos <- with(data, complete.cases(get(mediciones[1]), get(snps[2])) & complete.cases(get(snps[1])))
	valores <- with(data, matrix(unlist(mget(mediciones)), nrow(data), length(mediciones), dimnames=list(get(ids), mediciones)))
	promedios <- melt(aggregate(valores ~ get(snps[1]), FUN=mean, data=data), id.vars="get(snps[1])")
	names(promedios)[1] <- "genotipo"
	promedios$snp <- snps[1]
	promedios <- cbind(promedios, melt(aggregate(valores ~ get(snps[1]), FUN=function(x) sd(x)/sqrt(sum(condatos)), data=data), id.vars="get(snps[1])")[,3])
	names(promedios)[5] <- "et"
	promedios$genotipo <- factor(promedios$genotipo, labels=genotipos[,1])
	temp <- melt(aggregate(valores ~ get(snps[2]), FUN=mean, data=data), id.vars="get(snps[2])")
	names(temp)[1] <- "genotipo"
	temp$snp <- snps[2]
	temp <- cbind(temp, melt(aggregate(valores ~ get(snps[2]), FUN=function(x) sd(x)/sqrt(sum(condatos)), data=data), id.vars="get(snps[2])")[,3])
	temp$genotipo <- factor(temp$genotipo, labels=genotipos[,2])
	names(temp)[5] <- "et"
	promedios <- rbind(promedios, temp)
	promedios$geno <- NA
	temp <- melt(aggregate(valores ~ (get(snps[1])>0)+get(snps[2]), FUN=mean, data=data), id.vars=c("get(snps[2])", "get(snps[1]) > 0"))
	names(temp)[1] <- "genotipo"
	temp$genotipo <- factor(temp$genotipo, labels=genotipos[,2])
	names(temp)[2] <- "geno"
	temp$geno <- factor(temp$geno, labels=c(genotipos[1,1], paste(genotipos[2,1], "or", genotipos[3,1])))
	temp$snp <- paste(snps[1], snps[2], sep="x")
	temp <- cbind(temp, melt(aggregate(valores ~ (get(snps[1])>0) + get(snps[2]), FUN=function(x) sd(x)/sqrt(sum(condatos)), data=data), id.vars=c("get(snps[2])","get(snps[1]) > 0"))[,4])
	names(temp)[6] <- "et"
	promedios <- rbind(promedios, temp[,pmatch(names(promedios), names(temp))])
	promedios$snp <- factor(promedios$snp)
	promedios$geno <- factor(promedios$geno)
	promedios
	}

##obtenemos valores de asociación
covariables <- c("bmi", "sexo", "edad")

linear <- function(desenlace, snp, otros_predictores, base) {
	otros_predictores <- otros_predictores[!otros_predictores %in% desenlace]
	modelo <- as.formula(paste(desenlace, "~", paste(c(snp, otros_predictores), collapse=' + ')))
	if(is.numeric(with(base, get(desenlace))))
	{
		asoc <- coef(summary(lm(modelo, data=base)))[2,4]
	} else if (is.logical(with(base, get(desenlace))))
	{
		asoc <- coef(summary(glm(modelo, data=base, family="binomial")))[2,4]
	}
	asoc
}


interaccion <- function(desenlace, snps, otros_predictores, base) {
	otros_predictores <- otros_predictores[!otros_predictores %in% desenlace]
	modelo <- as.formula(paste(desenlace, "~", paste(snps, collapse="*"), "+", paste(otros_predictores, collapse=' + ')))
	if(is.numeric(with(base, get(desenlace))))
	{
		asoc <- coef(summary(lm(modelo, data=base)))[length(otros_predictores) + length(snps) +2,4]
	} else if (is.logical(with(base, get(desenlace))))
	{
		asoc <- coef(summary(glm(modelo, data=base, family="binomial")))[2,4]
	}
	asoc
}


variantes <- c("rs11722228", "rs2231142")


assign(variantes[1], sapply(fenos[-length(fenos)], function(x) linear(x, variantes[1], covariables, trab)))
model <- as.formula(paste("smidf", "~", paste(c(variantes[1], "sexo", "edad"), collapse=' + ')))
assign(variantes[1], c(get(variantes[1]), coef(summary(glm(model, data=trab, family="binomial")))[2,4]))
names(rs11722228)[length(rs11722228)] <- "smet"

assign(variantes[2], sapply(fenos[-length(fenos)], function(x) linear(x, variantes[2], covariables, trab)))
model <- as.formula(paste("smidf", "~", paste(c(variantes[2], "sexo", "edad"), collapse=' + ')))
assign(variantes[2], c(get(variantes[2]), coef(summary(glm(model, data=trab, family="binomial")))[2,4]))
names(rs2231142)[length(rs2231142)] <- "smet"

ef_combi <- sapply(fenos, function(x) interaccion(x, variantes, covariables, trab))



##gráfica de promedios de ácido úrico y otros metabólicos por genotipos de SLC2A9 en tres pasos; acomodo de datos, prueba de asociación y gráfica
##acomodo de datos
promedios <- datos_interaccion(trab, variantes, al, c(clave, imc_bioq), "folio")
##promedios$snp <- fct_recode(promedios$snp, rs11722228=levels(promedios$snp)[1], rs2231142=levels(promedios$snp)[3], rs11722228xrs2231142=levels(promedios$snp)[2])
promedios$snp <- fct_relevel(promedios$snp, "rs2231142", after=1)
promedios$genotipo <- fct_relevel(promedios$genotipo, "TT", after=Inf)
##promedios$genotipo <- fct_recode(promedios$genotipo, "AA " ="A A ", CA="CA ")
vars <- expand.grid(levels(promedios$variable), levels(promedios$snp))
colnames(vars) <- c("variable", "snp")
fenos <- c(clave, imc_bioq, "smet")
##creamos matriz de ubicación de valores de asociación dentro de la gráfica
loc <- matrix(c(rep(1.2, length(fenos)*3), rep(c(5.3, 100, 190, 46.3, 26.8, 0.54), 3)), length(fenos)*3, 2, dimnames=list(paste(rep(names(get(variantes[1])),3), rep(1:3, each=length(fenos))), c("x", "y")))
loc <- data.frame(loc, vars, val=c(rs11722228, rs2231142, ef_combi))
loc$geno=NA
loc$x[grep(clave, rownames(loc))] <- 2.7

##graficamos
dev.new(width=10, height=10)
pd=position_dodge(.15)
etiquetas <- c(urico = "uric acid (mg/dl)", glucosa = "glucose (mg/dl)", trigli = "triglycerides (mg/dl)", hdl = "HDL-c (mg/dl)", bmi = "BMI (kg/m²)", smidf="Metabolic s. (frequency)")
(interact_grafi <- ggplot(promedios, aes(x=genotipo, y=value, color=geno)) +
facet_grid(variable~snp, scales="free", labeller=labeller(variable=etiquetas)) +
geom_errorbar(aes(ymin=value-et, ymax=value+et), width=.15, position=pd, size=1.2) +
geom_point(position=pd, size=4, fill="black", shape=18) +
geom_text(aes(x,y, label=paste("p==", signif(val,4)), group=NULL), parse=TRUE, data=loc))

interact_grafi <- interact_grafi + labs(x=NULL, y=NULL)
interact_grafi <- interact_grafi + scale_colour_discrete(name="SLC2A9 genotype", breaks=levels(promedios$geno))
interact_grafi <- interact_grafi + theme(strip.text.x = element_text(size=12),
strip.text.y = element_text(size=8, face="bold"),
strip.background = element_rect(colour="red", fill="#CCCCFF"))
interact_grafi
ggsave(paste0("asociacion/graficas/",variantes[1], "_", variantes[2], "_urico_otrosmets.png"), width=10, height=10)
#####################################################################

